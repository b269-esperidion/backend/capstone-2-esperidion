const Product = require("../models/Product");



module.exports.addProduct = (data) => {
	// User is an admin
	if (data.isAdmin) {
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			quantity: data.product.quantity
		});
		// Saves the created object to our database
		return newProduct.save().then((product, error) => {
			
			if (error) {
				return false;
			
			} else {
				return true;
			};
		});
	};
	// User is not an admin
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};


// Retrieve ALL product
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};


// Retrieve ACTIVE product
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	});
};


// Retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Update a course
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price,
 		quantity: reqBody.quantity,
 		isActive: reqBody.isActive
	};

	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
};


module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};



