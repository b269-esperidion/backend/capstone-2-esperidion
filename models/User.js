const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	
	orders : [
		{ 
			products : [{
					productName: {
					type:String
				},
				
					quantity: {
					 type:Number
					}
				
			}],

			totalAmount: {
				type : Number
			},

			purchasedOn : {
				type : Date,
				// The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
				default : new Date()
			}
			
		}
	]
});


module.exports = mongoose.model("User", userSchema);