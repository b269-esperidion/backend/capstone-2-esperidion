const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");



router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		quantity: req.body.quantity,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the courses
router.get("/all" , (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving active courses
router.get("/active" , (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific course
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a course
router.put("/:productId", (req,res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Route to archiving a course
router.patch("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});



module.exports = router;


