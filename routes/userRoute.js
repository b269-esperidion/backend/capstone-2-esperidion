const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

// Route for checking if the user's email already exists in the database
const userController = require("../controllers/userController");

const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details


router.post("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

// Provides the user's ID for the getProfile controller method
userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});




router.post("/order", auth.verify, (req, res) => {
    let data = {
        // User ID will be retrieved from the request header
        userId: auth.decode(req.headers.authorization).id,
        // Course ID will be retrieved from the request body
        productId: req.body.productId
    }
    userController.order(data).then(resultFromController => res.send(resultFromController));
});

router.put("/:userId", (req,res) => {
	userController.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});
// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;




